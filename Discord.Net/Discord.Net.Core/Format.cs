using System.Text;

namespace Discord
{
    public static class Format
    {
        // Characters which need escaping
        private static readonly string[] SensitiveCharacters = { "\\", "*", "_", "~", "`", "|", ">" };

        public static string Bold(string text) => $"**{text}**";
        public static string Italics(string text) => $"*{text}*";
        public static string Underline(string text) => $"__{text}__";
        public static string Strikethrough(string text) => $"~~{text}~~";
        public static string Spoiler(string text) => $"||{text}||";
        public static string Url(string text, string url) => $"[{text}]({url})";
        public static string EscapeUrl(string url) => $"<{url}>";

        public static string Code(string text, string language = null)
        {
            if (language != null || text.Contains("\n"))
                return $"```{language ?? ""}\n{text}\n```";
            else
                return $"`{text}`";
        }

        public static string Sanitize(string text)
        {
            foreach (string unsafeChar in SensitiveCharacters)
                text = text.Replace(unsafeChar, $"\\{unsafeChar}");
            return text;
        }

        public static string Quote(string text)
        {
            // do not modify null or whitespace text
            // whitespace does not get quoted properly
            if (string.IsNullOrWhiteSpace(text))
                return text;

            StringBuilder result = new StringBuilder();

            int startIndex = 0;
            int newLineIndex;
            do
            {
                newLineIndex = text.IndexOf('\n', startIndex);
                if (newLineIndex == -1)
                {
                    // read the rest of the string
                    var str = text.Substring(startIndex);
                    result.Append($"> {str}");
                }
                else
                {
                    // read until the next newline
                    var str = text.Substring(startIndex, newLineIndex - startIndex);
                    result.Append($"> {str}\n");
                }
                startIndex = newLineIndex + 1;
            }
            while (newLineIndex != -1 && startIndex != text.Length);

            return result.ToString();
        }
        
        public static string BlockQuote(string text)
        {
            // do not modify null or whitespace
            if (string.IsNullOrWhiteSpace(text))
                return text;

            return $">>> {text}";
        }
    }
}
