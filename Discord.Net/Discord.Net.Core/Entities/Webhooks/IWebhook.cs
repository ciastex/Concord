﻿using System;
using System.Threading.Tasks;

namespace Discord
{
    public interface IWebhook : IDeletable, ISnowflakeEntity
    {
        string Name { get; }
        string Token { get; }
        string AvatarId { get; }

        ulong ChannelId { get; }
        ITextChannel Channel { get; }

        ulong? GuildId { get; }
        IGuild Guild { get; }

        IUser Creator { get; }

        string GetAvatarUrl(ImageFormat format = ImageFormat.Auto, ushort size = 128);
        Task ModifyAsync(Action<WebhookProperties> func, RequestOptions options = null);
    }
}
