namespace Discord
{
    public class VoiceChannelProperties : GuildChannelProperties
    {
        public Optional<int> Bitrate { get; set; }
        public Optional<int?> UserLimit { get; set; }
    }
}
