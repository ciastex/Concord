namespace Discord
{
    public class Emoji : IEmote
    {
        // TODO: need to constrain this to Unicode-only emojis somehow

        public string Name { get; }
        public override string ToString() => Name;

        public Emoji(string unicode)
        {
            Name = unicode;
        }

        public override bool Equals(object other)
        {
            if (other == null) return false;
            if (other == this) return true;

            var otherEmoji = other as Emoji;
            if (otherEmoji == null) return false;

            return string.Equals(Name, otherEmoji.Name);
        }

        public override int GetHashCode() => Name.GetHashCode();
    }
}
