using System.Diagnostics;

namespace Discord
{
    [DebuggerDisplay(@"{DebuggerDisplay,nq}")]
    public class MessageReference
    {
        public Optional<ulong> MessageId { get; internal set; }
        public Optional<ulong> GuildId { get; internal set; }
        
        public ulong ChannelId { get; internal set; }

        private string DebuggerDisplay
            => $"Channel ID: ({ChannelId}){(GuildId.IsSpecified ? $", Guild ID: ({GuildId.Value})" : "")}" +
            $"{(MessageId.IsSpecified ? $", Message ID: ({MessageId.Value})" : "")}";

        public override string ToString()
            => DebuggerDisplay;
    }
}
