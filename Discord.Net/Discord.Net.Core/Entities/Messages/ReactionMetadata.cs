namespace Discord
{
    public struct ReactionMetadata
    {
        public int ReactionCount { get; internal set; }
        public bool IsMe { get; internal set; }
    }
}
