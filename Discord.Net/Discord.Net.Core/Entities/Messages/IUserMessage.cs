using System;
using System.Threading.Tasks;

namespace Discord
{
    public interface IUserMessage : IMessage
    {
        Task ModifyAsync(Action<MessageProperties> func, RequestOptions options = null);
        Task ModifySuppressionAsync(bool suppressEmbeds, RequestOptions options = null);
        Task PinAsync(RequestOptions options = null);
        Task UnpinAsync(RequestOptions options = null);

        string Resolve(
            TagHandling userHandling = TagHandling.Name,
            TagHandling channelHandling = TagHandling.Name,
            TagHandling roleHandling = TagHandling.Name,
            TagHandling everyoneHandling = TagHandling.Ignore,
            TagHandling emojiHandling = TagHandling.Name);
    }
}
