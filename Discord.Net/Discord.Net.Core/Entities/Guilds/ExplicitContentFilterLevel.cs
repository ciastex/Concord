namespace Discord
{
    public enum ExplicitContentFilterLevel
    {
        Disabled = 0,
        MembersWithoutRoles = 1,
        AllMembers = 2
    }
}
