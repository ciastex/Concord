using System.Threading.Tasks;

namespace Discord
{
    public interface IUser : ISnowflakeEntity, IMentionable, IPresence
    {
        string AvatarId { get; }
        string Discriminator { get; }
        ushort DiscriminatorValue { get; }
        bool IsBot { get; }
        bool IsWebhook { get; }
        string Username { get; }

        string GetAvatarUrl(ImageFormat format = ImageFormat.Auto, ushort size = 128);
        string GetDefaultAvatarUrl();

        Task<IDMChannel> GetOrCreateDMChannelAsync(RequestOptions options = null);
    }
}
