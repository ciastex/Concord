using System.Collections.Immutable;

namespace Discord
{
    public interface IPresence
    {
        IActivity Activity { get; }
        UserStatus Status { get; }
        IImmutableSet<ClientType> ActiveClients { get; }
    }
}
