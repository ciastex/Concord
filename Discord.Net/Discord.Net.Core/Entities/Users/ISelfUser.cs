using System;
using System.Threading.Tasks;

namespace Discord
{
    public interface ISelfUser : IUser
    {
        string Email { get; }
        bool IsVerified { get; }
        bool IsMfaEnabled { get; }
        UserProperties Flags { get; }
        PremiumType PremiumType { get; }
        string Locale { get; }
        Task ModifyAsync(Action<SelfUserProperties> func, RequestOptions options = null);
    }
}
