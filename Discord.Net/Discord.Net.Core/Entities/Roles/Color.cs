using System;
using System.Diagnostics;
using StandardColor = System.Drawing.Color;

namespace Discord
{
    [DebuggerDisplay(@"{DebuggerDisplay,nq}")]
    public struct Color
    {
        public static readonly Color Default = new Color(0);
        public static readonly Color Teal = new Color(0x1ABC9C);
        public static readonly Color DarkTeal = new Color(0x11806A);
        public static readonly Color Green = new Color(0x2ECC71);
        public static readonly Color DarkGreen = new Color(0x1F8B4C);
        public static readonly Color Blue = new Color(0x3498DB);
        public static readonly Color DarkBlue = new Color(0x206694);
        public static readonly Color Purple = new Color(0x9B59B6);
        public static readonly Color DarkPurple = new Color(0x71368A);
        public static readonly Color Magenta = new Color(0xE91E63);
        public static readonly Color DarkMagenta = new Color(0xAD1457);
        public static readonly Color Gold = new Color(0xF1C40F);
        public static readonly Color LightOrange = new Color(0xC27C0E);
        public static readonly Color Orange = new Color(0xE67E22);
        public static readonly Color DarkOrange = new Color(0xA84300);
        public static readonly Color Red = new Color(0xE74C3C);
        public static readonly Color DarkRed = new Color(0x992D22);
        public static readonly Color LightGrey = new Color(0x979C9F);
        public static readonly Color LighterGrey = new Color(0x95A5A6);
        public static readonly Color DarkGrey = new Color(0x607D8B);
        public static readonly Color DarkerGrey = new Color(0x546E7A);
        
        public uint RawValue { get; }

        public byte R => (byte)(RawValue >> 16);
        public byte G => (byte)(RawValue >> 8);
        public byte B => (byte)(RawValue);

        public Color(uint rawValue)
        {
            RawValue = rawValue;
        }

        public Color(byte r, byte g, byte b)
        {
            RawValue =
                ((uint)r << 16) |
                ((uint)g << 8) |
                (uint)b;
        }

        public Color(int r, int g, int b)
        {
            if (r < 0 || r > 255)
                throw new ArgumentOutOfRangeException(nameof(r), "Value must be within [0,255].");
            if (g < 0 || g > 255)
                throw new ArgumentOutOfRangeException(nameof(g), "Value must be within [0,255].");
            if (b < 0 || b > 255)
                throw new ArgumentOutOfRangeException(nameof(b), "Value must be within [0,255].");
            RawValue =
                ((uint)r << 16) |
                ((uint)g << 8) |
                (uint)b;
        }

        public Color(float r, float g, float b)
        {
            if (r < 0.0f || r > 1.0f)
                throw new ArgumentOutOfRangeException(nameof(r), "Value must be within [0,1].");
            if (g < 0.0f || g > 1.0f)
                throw new ArgumentOutOfRangeException(nameof(g), "Value must be within [0,1].");
            if (b < 0.0f || b > 1.0f)
                throw new ArgumentOutOfRangeException(nameof(b), "Value must be within [0,1].");
            RawValue =
                ((uint)(r * 255.0f) << 16) |
                ((uint)(g * 255.0f) << 8) |
                (uint)(b * 255.0f);
        }

        public static bool operator ==(Color lhs, Color rhs)
            => lhs.RawValue == rhs.RawValue;

        public static bool operator !=(Color lhs, Color rhs)
            => lhs.RawValue != rhs.RawValue;

        public override bool Equals(object obj)
            => (obj is Color c && RawValue == c.RawValue);

        public override int GetHashCode() => RawValue.GetHashCode();

        public static implicit operator StandardColor(Color color) =>
            StandardColor.FromArgb((int)color.RawValue);
        public static explicit operator Color(StandardColor color) =>
            new Color((uint)color.ToArgb() << 8 >> 8);

        public override string ToString() =>
            string.Format("#{0:X6}", RawValue);
        private string DebuggerDisplay =>
            string.Format("#{0:X6} ({0})", RawValue);
    }
}
