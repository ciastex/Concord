using Discord.Net.Udp;
using Discord.Net.WebSockets;
using Discord.Rest;

namespace Discord.WebSocket
{
    public class DiscordSocketConfig : DiscordRestConfig
    {
        public const string GatewayEncoding = "json";

        public string GatewayHost { get; set; } = null;
        public int ConnectionTimeout { get; set; } = 30000;
        public int? ShardId { get; set; } = null;
        public int? TotalShards { get; set; } = null;
        public int MessageCacheSize { get; set; } = 0;
        public int LargeThreshold { get; set; } = 250;
        public WebSocketProvider WebSocketProvider { get; set; }
        public UdpSocketProvider UdpSocketProvider { get; set; }
        public bool AlwaysDownloadUsers { get; set; } = false;
        public int? HandlerTimeout { get; set; } = 3000;
        public bool? ExclusiveBulkDelete { get; set; } = null;
        public bool GuildSubscriptions { get; set; } = true;

        public DiscordSocketConfig()
        {
            WebSocketProvider = DefaultWebSocketProvider.Instance;
            UdpSocketProvider = DefaultUdpSocketProvider.Instance;
        }

        internal DiscordSocketConfig Clone() => MemberwiseClone() as DiscordSocketConfig;
    }
}
