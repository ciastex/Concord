namespace Discord.Rest
{
    public struct GuildInfo
    {
        public int? AfkTimeout { get; }
        public DefaultMessageNotifications? DefaultMessageNotifications { get; }
        public ulong? AfkChannelId { get; }
        public string Name { get; }
        public string RegionId { get; }
        public string IconHash { get; }
        public VerificationLevel? VerificationLevel { get; }
        public IUser Owner { get; }
        public MfaLevel? MfaLevel { get; }
        public ExplicitContentFilterLevel? ExplicitContentFilter { get; }
        public ulong? SystemChannelId { get; }
        public ulong? EmbedChannelId { get; }
        public bool? IsEmbeddable { get; }

        internal GuildInfo(int? afkTimeout, DefaultMessageNotifications? defaultNotifs,
            ulong? afkChannel, string name, string region, string icon,
            VerificationLevel? verification, IUser owner, MfaLevel? mfa, ExplicitContentFilterLevel? filter,
            ulong? systemChannel, ulong? widgetChannel, bool? widget)
        {
            AfkTimeout = afkTimeout;
            DefaultMessageNotifications = defaultNotifs;
            AfkChannelId = afkChannel;
            Name = name;
            RegionId = region;
            IconHash = icon;
            VerificationLevel = verification;
            Owner = owner;
            MfaLevel = mfa;
            ExplicitContentFilter = filter;
            SystemChannelId = systemChannel;
            EmbedChannelId = widgetChannel;
            IsEmbeddable = widget;
        }
    }
}
