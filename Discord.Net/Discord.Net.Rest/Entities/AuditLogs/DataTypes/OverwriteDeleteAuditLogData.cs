using System.Linq;

using Model = Discord.API.AuditLog;
using EntryModel = Discord.API.AuditLogEntry;

namespace Discord.Rest
{
    public class OverwriteDeleteAuditLogData : IAuditLogData
    {
        public ulong ChannelId { get; }
        public Overwrite Overwrite { get; }

        private OverwriteDeleteAuditLogData(ulong channelId, Overwrite deletedOverwrite)
        {
            ChannelId = channelId;
            Overwrite = deletedOverwrite;
        }

        internal static OverwriteDeleteAuditLogData Create(BaseDiscordClient discord, Model log, EntryModel entry)
        {
            var changes = entry.Changes;

            var denyModel = changes.FirstOrDefault(x => x.ChangedProperty == "deny");
            var typeModel = changes.FirstOrDefault(x => x.ChangedProperty == "type");
            var idModel = changes.FirstOrDefault(x => x.ChangedProperty == "id");
            var allowModel = changes.FirstOrDefault(x => x.ChangedProperty == "allow");

            var deny = denyModel.OldValue.ToObject<ulong>(discord.ApiClient.Serializer);
            var type = typeModel.OldValue.ToObject<PermissionTarget>(discord.ApiClient.Serializer);
            var id = idModel.OldValue.ToObject<ulong>(discord.ApiClient.Serializer);
            var allow = allowModel.OldValue.ToObject<ulong>(discord.ApiClient.Serializer);

            return new OverwriteDeleteAuditLogData(entry.TargetId.Value, new Overwrite(id, type, new OverwritePermissions(allow, deny)));
        }
    }
}
