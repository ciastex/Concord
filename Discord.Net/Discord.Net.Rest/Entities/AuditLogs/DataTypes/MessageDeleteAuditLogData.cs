using Model = Discord.API.AuditLog;
using EntryModel = Discord.API.AuditLogEntry;

namespace Discord.Rest
{
    public class MessageDeleteAuditLogData : IAuditLogData
    {
        public int MessageCount { get; }
        public ulong ChannelId { get; }
        public ulong AuthorId { get; }

        private MessageDeleteAuditLogData(ulong channelId, int count, ulong authorId)
        {
            ChannelId = channelId;
            MessageCount = count;
            AuthorId = authorId;
        }

        internal static MessageDeleteAuditLogData Create(BaseDiscordClient discord, Model log, EntryModel entry)
        {
            return new MessageDeleteAuditLogData(entry.Options.MessageDeleteChannelId.Value, entry.Options.MessageDeleteCount.Value, entry.TargetId.Value);
        }
    }
}
