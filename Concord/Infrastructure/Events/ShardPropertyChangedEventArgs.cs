﻿using System;

namespace Concord.Infrastructure.Events
{
    public class ShardPropertyChangedEventArgs : EventArgs
    {
        public string PropertyName { get; }
        public object Value { get; }

        public ShardPropertyChangedEventArgs(string propertyName, object value)
        {
            PropertyName = propertyName;
            Value = value;
        }
    }
}