﻿using System;
using System.Windows.Markup;
using DevExpress.Mvvm;

namespace Concord.Infrastructure.Mvvm
{
    [MarkupExtensionReturnType(typeof(ViewModelBase))]
    public class DataContextSource : MarkupExtension
    {
        [ConstructorArgument("viewModelType")]
        public Type ViewModelType { get; set; }

        public DataContextSource(Type viewModelType)
        {
            ViewModelType = viewModelType;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return ViewModelLocator.Default.ResolveViewModel(ViewModelType.Name);
        }
    }
}