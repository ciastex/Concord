﻿using Discord;
using System.Collections.Generic;

namespace Concord.Infrastructure.Messages.Discord
{
    public class UpdateDirectMessageChannelList
    {
        public List<IDMChannel> Channels { get; }

        public UpdateDirectMessageChannelList(List<IDMChannel> channels)
        {
            Channels = channels;
        }
    }
}
