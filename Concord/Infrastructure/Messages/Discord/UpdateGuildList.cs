﻿using System.Collections.Generic;
using Discord;

namespace Concord.Infrastructure.Messages.Discord
{
    public class UpdateGuildList
    {
        public List<IGuild> Guilds { get; }

        public UpdateGuildList(List<IGuild> guilds)
        {
            Guilds = guilds;
        }
    }
}