﻿using Concord.Infrastructure.Configuration.Shards;
using DevExpress.Mvvm;

namespace Concord.ViewModel.Windows
{
    public class SettingsWindowViewModel : ViewModelBase
    {
        public DiscordConfigShard Discord { get; }

        public bool ShowDiscordToken { get; set; }

        public SettingsWindowViewModel()
        {
            Discord = new DiscordConfigShard();
        }
    }
}