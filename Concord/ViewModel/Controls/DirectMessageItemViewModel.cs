﻿using Concord.Infrastructure.Messages.Discord;
using Concord.Services;
using Concord.Services.Interfaces;
using DevExpress.Mvvm;
using Discord;
using System.Threading.Tasks;

namespace Concord.ViewModel.Controls
{
    public class DirectMessageItemViewModel : ChatItemViewModelBase
    {
        private readonly DiscordService _discord;

        public IDMChannel Channel { get; }
        public IUser Recipient => Channel.Recipient;

        public string FullUserName => $"{Recipient.Username}#{Recipient.Discriminator}";
        public string Status => $"{Recipient.Status}";

        public DirectMessageItemViewModel()
        {
            _discord = GetService<IDiscordService>() as DiscordService;
            Messenger.Default.Register<UpdatePresence>(this, OnUpdatePresence);
        }

        public DirectMessageItemViewModel(IDMChannel channel)
            : this()
        {
            Channel = channel;

            Task.Run(
                async () =>
                {
                    await DownloadServerImageAsync(Recipient.GetAvatarUrl(ImageFormat.Png));
                }
            );
        }

        private void OnUpdatePresence(UpdatePresence message)
        {
            if (message.User.Id == Channel.Recipient.Id)
            {
                RaisePropertiesChanged(nameof(FullUserName), nameof(Status));
            }
        }
    }
}
